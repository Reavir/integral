from sympy import *
import numpy as np
import time
init_printing()
x, y, z = symbols('x y z')


def inp_fun():
    dim = int(input("Number of variables: "))
    if dim == 1:
        b = input("Upper bound: ")
        a = input("Lower bound: ")
        f = input("Function: ")
        pprint(Integral(f, (x, a, b)), use_unicode=False)
        return b, a, f


def trapezoidal_singular(f, a, b, N):
    h = (b-a)/N
    xi = np.linspace(a, b, N + 1)
    fi = f(xi)
    ss = 0.0
    for i in range(1, N):
        ss += fi[i]
    s = (h/2)*(fi[0] + fi[N]) + h*ss
    return s


def trapezoidal_double(f, a, b, c, d, nx, ny):
    hx = (b - a)/float(nx)
    hy = (d - c)/float(ny)
    I = 0.25*(f(a, c) + f(a, d) + f(b, c) + f(b, d))
    Ix = 0
    for i in range(1, nx):
        xi = a + i*hx
        Ix += f(xi, c) + f(xi, d)
    I += 0.5*Ix
    Iy = 0
    for j in range(1, ny):
        yj = c + j*hy
        Iy += f(a, yj) + f(b, yj)
    I += 0.5*Iy
    Ixy = 0
    for i in range(1, nx):
        for j in range(1, ny):
            xi = a + i*hx
            yj = c + j*hy
            Ixy += f(xi, yj)
    I += Ixy
    I *= hx*hy
    return I


def trapezoidal_triple(f, a, b, c, d, e, g, nx, ny, nz):
    hx = (b - a)/float(nx)
    hy = (d - c)/float(ny)
    hz = (g - e)/float(nz)

    I = 0.125*(f(a, c, e) + f(a, d, g) + f(a, c, g) + f(a, d, e) + f(b, c, e) + f(b, d, g) + f(b, c, g) + f(b, d, e))

    Ix = 0
    for i in range(1, nx):
        xi = a + i*hx
        Ix += f(xi, c, e) + f(xi, d, g) + f(xi, c, g) + f(xi, d, e)
    I += 0.25*Ix

    Iy = 0
    for j in range(1, ny):
        yj = c + j*hy
        Iy += f(a, yj, e) + f(b, yj, g) + f(a, yj, g) + f(b, yj, e)
    I += 0.25*Iy

    Iz = 0
    for k in range(1, nz):
        zk = e + k*hz
        Iz += f(a, c, zk) + f(b, d, zk) + f(a, d, zk) + f(b, c, zk)
    I += 0.25*Iz

    Ixy = 0
    for i in range(1, nx):
        for j in range(1, ny):
            xi = a + i * hx
            yj = c + j * hy
            Ixy += f(xi, yj, e) + f(xi, yj, g)
    I += 0.5*Ixy

    Ixz = 0
    for i in range(1, nx):
        for k in range(1, nz):
            xi = a + i * hx
            zk = e + k * hz
            Ixz += f(xi, c, zk) + f(xi, d, zk)
    I += 0.5*Ixz

    Iyz = 0
    for j in range(1, ny):
        for k in range(1, nz):
            yj = c + j * hy
            zk = e + k * hz
            Iyz += f(a, yj, zk) + f(b, yj, zk)
    I += 0.5*Iyz

    Ixyz = 0
    for i in range(1, nx):
        for j in range(1, ny):
            for k in range(1, nz):
                xi = a + i*hx
                yj = c + j*hy
                zk = e + k*hz
                Ixyz += f(xi, yj, zk)
    I += Ixyz
    I *= hx*hy*hz
    return I


def trapezoidal_quadruple(f, a, b, c, d, e, g, h, m, nx, ny, nz, na):
    hx = (b - a)/float(nx)
    hy = (d - c)/float(ny)
    hz = (g - e)/float(nz)
    ha = (m - h)/float(na)

    I = 0.0625*(f(a, c, e, h) + f(a, d, g, h) + f(a, c, g, h) + f(a, d, e, h) + f(b, c, e, h) + f(b, d, g, h) + f(b, c, g, h) + f(b, d, e, h)\
                + f(a, c, e, m) + f(a, d, g, m) + f(a, c, g, m) + f(a, d, e, m) + f(b, c, e, m) + f(b, d, g, m) + f(b, c, g, m) + f(b, d, e, m))

    Ix = 0
    for i in range(1, nx):
        xi = a + i*hx
        Ix += f(xi, c, e, h) + f(xi, d, g, h) + f(xi, c, g, h) + f(xi, d, e, h)\
              + f(xi, c, e, m) + f(xi, d, g, m) + f(xi, c, g, m) + f(xi, d, e, m)
    I += 0.125*Ix

    Iy = 0
    for j in range(1, ny):
        yj = c + j*hy
        Iy += f(a, yj, e, h) + f(b, yj, g, h) + f(a, yj, g, h) + f(b, yj, e, h)\
              + f(a, yj, e, m) + f(b, yj, g, m) + f(a, yj, g, m) + f(b, yj, e, m)
    I += 0.125*Iy

    Iz = 0
    for k in range(1, nz):
        zk = e + k*hz
        Iz += f(a, c, zk, h) + f(b, d, zk,  h) + f(a, d, zk, h) + f(b, c, zk, h)\
              + f(a, c, zk, m) + f(b, d, zk,  m) + f(a, d, zk, m) + f(b, c, zk, m)
    I += 0.125*Iz

    Ia = 0
    for l in range(1, nz):
        al = h + l*ha
        Ia += f(a, c, e, al) + f(b, d, e, al) + f(a, d, e, al) + f(b, c, e, al)\
              + f(a, c, g, al) + f(b, d, g, al) + f(a, d, g, al) + f(b, c, g, al)
    I += 0.125*Ia

    Ixy = 0
    for i in range(1, nx):
        for j in range(1, ny):
            xi = a + i * hx
            yj = c + j * hy
            Ixy += f(xi, yj, e, h) + f(xi, yj, g, h) + f(xi, yj, e, m) + f(xi, yj, g, m)

    I += 0.25*Ixy

    Ixz = 0
    for i in range(1, nx):
        for k in range(1, nz):
            xi = a + i * hx
            zk = e + k * hz
            Ixz += f(xi, c, zk, h) + f(xi, d, zk, h) + f(xi, c, zk, m) + f(xi, d, zk, m)
    I += 0.25*Ixz

    Iyz = 0
    for j in range(1, ny):
        for k in range(1, nz):
            yj = c + j * hy
            zk = e + k * hz
            Iyz += f(a, yj, zk, h) + f(b, yj, zk, h) + f(a, yj, zk, m) + f(b, yj, zk, m)
    I += 0.25*Iyz

    Ixa = 0
    for i in range(1, nx):
        for l in range(1, na):
            xi = a + i * hx
            al = h + l * ha
            Ixa += f(xi, c, e, al) + f(xi, c, g, al) + f(xi, d, e, al) + f(xi, d, g, al)
    I += 0.25*Ixa

    Iya = 0
    for j in range(1, ny):
        for l in range(1, na):
            yj = c + j * hy
            al = h + l * ha
            Iya += f(a, yj, e, al) + f(a, yj, g, al) + f(b, yj, e, al) + f(b, yj, g, al)
    I += 0.25*Iya

    Iza = 0
    for k in range(1, nz):
        for l in range(1, na):
            zk = e + k * hz
            al = h + l * ha
            Iza += f(a, c, zk, al) + f(b, c, zk, al) + f(a, d, zk, al) + f(b, d, zk, al)
    I += 0.25*Iza

    Ixyz = 0
    for i in range(1, nx):
        for j in range(1, ny):
            for k in range(1, nz):
                xi = a + i*hx
                yj = c + j*hy
                zk = e + k*hz
                Ixyz += f(xi, yj, zk, h) + f(xi, yj, zk, m)
    I += 0.5*Ixyz

    Ixya = 0
    for i in range(1, nx):
        for j in range(1, ny):
            for l in range(1, na):
                xi = a + i*hx
                yj = c + j*hy
                al = h + l * ha
                Ixya += f(xi, yj, e, al) + f(xi, yj, g, al)
    I += 0.5*Ixya

    Ixza = 0
    for i in range(1, nx):
        for k in range(1, nz):
            for l in range(1, na):
                xi = a + i*hx
                zk = e + k*hz
                al = h + l * ha
                Ixza += f(xi, c, zk, al) + f(xi, d, zk, al)
    I += 0.5*Ixza

    Iyza = 0
    for j in range(1, ny):
        for k in range(1, nz):
            for l in range(1, na):
                yj = c + j*hy
                zk = e + k*hz
                al = h + l * ha
                Iyza += f(a, yj, zk, al) + f(b, yj, zk, al)
    I += 0.5*Iyza

    Ixyza = 0
    for i in range(1, nx):
        for j in range(1, ny):
            for k in range(1, nz):
                for l in range(1, na):
                    xi = a + i*hx
                    yj = c + j*hy
                    zk = e + k*hz
                    al = h + l * ha
                    Ixyza += f(xi, yj, zk, al)
    I += Ixyza

    I *= hx*hy*hz*ha
    return I


def romberg_singular(f, a, b, eps ,nmax):
    # f     ... function to be integrated
    # [a,b] ... integration interval
    # eps   ... desired accuracy
    # nmax  ... maximal order of Romberg method
    T = np.zeros((nmax, nmax), float)
    for i in range(0, nmax):
        N = 2**i
        T[i, 0] = trapezoidal_singular(f, a, b, N)
        for k in range(1, i):
            T[i, k] = (4**k*T[i, k-1] - T[i-1, k-1]) / (4**k-1)
            if i > 0:
                if abs(T[i, k] - T[i, k-1]) < eps:
                    break
    print(T[i, k])
    return T[i, k]


def romberg_double(f, a, b, c, d, eps ,nmax):
    # f     ... function to be integrated
    # [a,b] ... integration interval
    # eps   ... desired accuracy
    # nmax  ... maximal order of Romberg method
    T = np.zeros((nmax, nmax), float)
    for i in range(0, nmax):
        N = 2**i
        T[i, 0] = trapezoidal_double(f, a, b, c, d, N, N)
        for k in range(1, i):
            T[i, k] = (4**k*T[i, k-1] - T[i-1, k-1]) / (4**k-1)
            if i > 0:
                if abs(T[i, k] - T[i, k-1]) < eps:
                    break
    print(T[i, k])
    return T[i, k]


def romberg_triple(f, a, b, c, d, e, g, eps ,nmax):
    # f     ... function to be integrated
    # [a,b] ... integration interval
    # eps   ... desired accuracy
    # nmax  ... maximal order of Romberg method
    T = np.zeros((nmax, nmax), float)
    for i in range(0, nmax):
        N = 2**i
        T[i, 0] = trapezoidal_triple(f, a, b, c, d, e, g, N, N, N)
        for k in range(1, i):
            T[i, k] = (4**k*T[i, k-1] - T[i-1, k-1]) / (4**k-1)
            if i > 0:
                if abs(T[i, k] - T[i, k-1]) < eps:
                    break
    print(T[i, k])
    return T[i, k]


def romberg_quadruple(f, a, b, c, d, e, g, h, m, eps ,nmax):
    # f     ... function to be integrated
    # [a,b] ... integration interval
    # eps   ... desired accuracy
    # nmax  ... maximal order of Romberg method
    T = np.zeros((nmax, nmax), float)
    for i in range(0, nmax):
        N = 2**i
        T[i, 0] = trapezoidal_quadruple(f, a, b, c, d, e, g, h, m, N, N, N, N)
        for k in range(1, i):
            T[i, k] = (4**k*T[i, k-1] - T[i-1, k-1]) / (4**k-1)
            if i > 0:
                if abs(T[i, k] - T[i, k-1]) < eps:
                    break
    print(T[i, k])
    return T[i, k]


def f1(x):
    f1 = x**2
    return f1


def f2(x, y):
    f2 = np.sin(x+y)
    return f2


def f3(x, y, z):
    f3 = np.sin(x+y+z)
    return f3


def f4(x, y, z, a):
    f4 = np.sin(x+y+z+a)
    return f4


#bounds
'''x'''
a = 0
b = 1
'''y'''
c = 0
d = 2
'''z'''
e = 1
g = 5
'''a'''
h = 0
m = 9

start_1 = time.time()
romberg_singular(f1, a, b, 1.0e-12, 7)
end_1 = time.time()
print("elapsed time", end_1 - start_1)

start_2 = time.time()
romberg_double(f2, a, b, c, d, 1.0e-12, 7)
end_2 = time.time()
print("elapsed time", end_2 - start_2)


start_3 = time.time()
romberg_triple(f3, a, b, c, d, e, g, 1.0e-12, 7)
end_3 = time.time()
print("elapsed time", end_3 - start_3)


start_4 = time.time()
romberg_quadruple(f4, a, b, c, d, e, g, h, m, 1.0e-12, 7)
end_4 = time.time()
print("elapsed time", end_4 - start_4)



# sin(x+y)
# wolfram alfa
# 1.609 648 403 573 710 9
# romberg
# 1.609 648 403 573 067 8 (order 10)

# sin(x+y+z)
# wolfram alfa
# -2.868 708 407 491 173 5
# romberg
# -2.868 708 407 491 274 (order 7)

# sin(x+y+z+a)
# wolfram alfa
# -2.364 495 526 991 108 4
# romberg
# -2.364 495 527 009 7827 (order 7)
